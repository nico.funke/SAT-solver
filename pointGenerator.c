/**************************************************
 * 		Structur and methods to generate 
 * 		the points for local search
 * 		--------------------------------
 *  @author Nico Funke
 							2017
 ***********************************************/
#include "pointGenerator.h"

/**
 * saves a pointer to the matrix generator 
 * and allocates Memory on Heap
 **/
void newPointGenerator(pointGenerator_t *p, matrix_t *generator){
	p->generator = generator;
	p->binVector = createFullVector( generator->n, FALSE );
	p->solution = createFullVector( generator->m, FALSE );
}

/**
 * Frees the allocated memory
 * Attention: Frees also the matrix
 **/
void freePointGenerator(pointGenerator_t *p){
	free(p->binVector);
	free(p->solution);
	freeMatrix(p->generator);
}

/**
 * Increments the vector and saves the new point under solution
 * Returns FALSE if there are no more points
 **/
int getNextPoint(pointGenerator_t *p, int *solution,int printHelp){
	int i = incrementVector(p->binVector, p->generator->n);
	if(printHelp){
		printVector(p->binVector,p->generator->n);
	}
	//printf("%d\n", 40-i);
	if(i == -1){	//Vector can not be incremented
		return FALSE;
	}
	addVectorsMod2(p->generator->field[i],p->solution,solution,p->generator->m); 
	return TRUE;
}
