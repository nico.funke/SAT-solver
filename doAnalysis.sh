make;
echo "------------"
echo "Solving first problem...";
./solvingSat s 148526678 r 0 > analysis.txt;
gprof solvingSat gmon.out >> analysis.txt;
echo "Solving second problem...";
./solvingSat s 1485267901 t 50 10 60 70 r 0 > analysis2.txt;
gprof solvingSat gmon.out >> analysis2.txt;
echo "Solving third problem..."
./solvingSat s 1485267972 t 70 10 90 100 r 0 > analysis3.txt;
gprof solvingSat gmon.out >> analysis3.txt;
echo "Done.";
