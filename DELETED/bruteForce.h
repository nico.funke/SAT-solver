
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include "myCNF.h"
#include "myMatrix.h"

#define SHOWWAY		0

int bruteForceSolve(CNF_t cnf, int *solution, int *steps);