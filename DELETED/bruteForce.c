/*****************************************************
 *		  solving SAT with bruteforce
 * 		---------------------------------
 *	@author Nico Funke
 *								2016
 *****************************************************/

#include "myCNF.h"
#include "myMatrix.h"
#include "bruteForce.h"


/**
 * Solves CNF with bruteForce
 * -returns 1, if a solution was found
 **/
int bruteForceSolve(CNF_t cnf, int *solution, int *steps){
	int i = 0;
	//Start with 0...00;
	while(i < cnf.variables){
		solution[i] = 0;
		i++;
	}

	//Test all possibilities
	i=0;
	do{
		if(SHOWWAY){
			printf("BruteForce testet: ");
			printVector(solution, cnf.variables);
		}
		(*steps)++;
		if(isSolution(cnf, solution)){
			return 1;
		}
	}while(incrementVector(solution, cnf.variables));
	return 0;
}

