/***********************************
 * Second Route
 *********************************/

 //===== PROTOTYPES ==============================
void freeRoute(route_t *route);
void newRoute(route_t *route, int variables);
int pointToInt(int *point, int length);
int isContained(route_t *route, int *solution);
int pointToIntWithBitFlipped(int *point, int length, int change);
int vectorListEquals(vectorList_t vector, int *solution);
void freeVectorList(vectorList_t vector);
void newVectorList( vectorList_t vector, int length, int *solution);

 //===== STRUCTS ==============================
struct vectorList_s {
	int length;
	int *field;
	struct vectorList_s *next;
};

typedef struct vectorList_s *vectorList_t;

typedef struct {
	int variables;
	vectorList_t *buckets;
} route_t;

 //===== METHODS FOR VECTORLIST ==============================
/**
 * Checks if the vector in Vectorlist euqals the int array
 **/
int vectorListEquals(vectorList_t vector, int *solution){
	int i = 0;
	while(i < vector->length){
		if(vector->field[i] != solution[i]){
			return FALSE;
		}
		i++;
	}
	return TRUE;
}

/**
 * Frees the heap 
 **/
void freeVectorList(vectorList_t vector){
	if(vector == NULL){
		return;
	}else{
		freeVectorList(vector->next);
		free(vector->field);
		free(vector);
	}
}

/**
 * Creates a new VectorList in vector pointer
 * - allocates memory on heap
 **/
void newVectorList( vectorList_t vector, int length, int *solution){
	vector->next = NULL;
	vector->length = length;
	vector->field = (int *) malloc ( sizeof(int) * length);
	int i = 0;
	while(i < length){
		vector->field[i] = solution[i];
		i++;
	}
}

//===== METHODS FOR ROUTE ===================================

/**
 * Checks whether the route contains the point
 * and adds the point if it is not contained yet
 **/
int isContained(route_t *route, int *solution){
	int intrepres = pointToInt(solution, route->variables);
	vectorList_t lastVector = route->buckets[intrepres];
	vectorList_t tmpVector = NULL;
	if( lastVector != NULL){
		tmpVector = lastVector->next; 
	}

	if(lastVector != NULL){
		// ---bucket is filled
		int i = 0;

		//---check first point
		if(vectorListEquals(lastVector, solution)){
				return TRUE;		//found value!
		}
		//---check other points
		while( tmpVector != NULL ){
			if(vectorListEquals(tmpVector, solution)){
				return TRUE;		//found value!
			}
			lastVector = tmpVector;
			tmpVector = tmpVector->next;
		}	
		 // ---Value was not found
		tmpVector = (vectorList_t ) malloc(sizeof(struct vectorList_s));
		newVectorList(tmpVector, route->variables, solution);
		lastVector->next = tmpVector;

		return FALSE;

	} else { // ---bucket is empty
		lastVector = (vectorList_t ) malloc(sizeof(struct vectorList_s));
		newVectorList( lastVector, route->variables,solution);
		route->buckets[intrepres] = lastVector;
		return FALSE;
	}	
}
	

/**
 * creates new route with memory on the heap
 **/
void newRoute(route_t *route, int variables){
	route->variables = variables;
	route->buckets = (vectorList_t *) malloc(sizeof(vectorList_t) * PRIME);
	int i = 0;
	while( i < PRIME){
		route->buckets[i] = NULL;
		i++;
	}
}

/**
 * frees heap
 */
void freeRoute(route_t *route){
	int i = 0;
	while(i < PRIME){
		freeVectorList((route->buckets[i]));
		i++;
	}
	free(route->buckets);
}

/**
 * creates integer representation 
 * - modulo prime
 **/
int pointToInt(int *point, int length){
	int output = 0, i = 0;
	while(i < length){
		output *= 2;
		output += point[i];
		output = output%PRIME;
		i++;
	}
	return output;
}

/**
 * creates integer representation 
 * -modulo prime
 **/
int pointToIntWithBitFlipped(int *point, int length, int change){
	int output = 0, i = 0;
	while(i < length){
		output *= 2;
		if(i == change){
			output += (point[i]+1)%2; 	//flipped bit
		}else{
			output += point[i];			//normal bit
		}
		output = output%PRIME;
		i++;
	}
	return output;
}