/***********************************
 * First Route
 * Max. 32 vars
 *********************************/

/**
 * To save visited points
 **/
typedef struct {
	int variables,length;
	int *points;
} route_t;

/**
 * Checks whether the route contains the point
 **/
int isContained(route_t route, int intRepres){
	int i = 0;
	while( i < route.length){
		if(route.points[i] == intRepres){
			return 1;
		}
		i++;
	}
	return 0;
}

/**
 * creates new route with memory on the heap
 **/
void newRoute(route_t *route, int variables,int depth){
	route->variables = variables;
	route->length = 0;
	route->points = (int *) malloc(sizeof(int) * 1<<(depth+8));
}

/**
 * frees heap
 */
void freeRoute(route_t *route){
	free(route->points);
}

/**
 * creates integer representation 
 **/
int pointToInt(int *point, int length){
	int output = 0, i = 0;
	while(i < length){
		output *= 2;
		output += point[i];
		i++;
	}
	return output;
}

/**
 * creates integer representation 
 **/
int pointToIntWithBitFlipped(int *point, int length, int change){
	int output = 0, i = 0;
	while(i < length){
		output *= 2;
		if(i == change){
			output += (point[i]+1)%2; 	//flipped bit
		}else{
			output += point[i];			//normal bit
		}
		i++;
	}
	return output;
}

/**
 * adds point to the route
 * -saves points as two's complement interpretation
 **/
void addPoint(route_t *route, int *point){
	if(route->length >= 1<<(6+8)){ printf("Ueberschritten!\n");} // TODO
	int i = 0, j = route->length;
	route->length ++;
	route->points[j] = pointToInt(point, route->variables);
}