
#ifndef myQueue
#define myQueue

typedef struct {
  struct queue_node_s *next;
  int *data;
} myQueueNode;

typedef struct{
  struct myQueueNode *start;
  struct myQueueNode *end;
} myQueue;

void freeQueue(myQueue *queue);
void newQueue(myQueue *queue);
void enqueue(myQueue *queue, int *input);
void *dequeue(myQueue *queue, int* output);
int isEmpty(myQueue *queue);

#endif