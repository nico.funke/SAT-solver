#include <stdio.h>
#include <stdlib.h>


#define TRUE		  1 
#define FALSE		  0
#define NOTCONTAINED -1

#ifndef pointGenerator
#define pointGenerator
#include "myMatrix.h"

typedef struct{
	matrix_t *generator;
	int *binVector;
	int *solution;
} pointGenerator_t;




void newPointGenerator(pointGenerator_t *p, matrix_t *generator);
void freePointGenerator(pointGenerator_t *p);
int getNextPoint(pointGenerator_t *p, int *solution,int printHelp);
#endif