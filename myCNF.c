/*****************************************************
 *		Operations with CNFs
 * 		---------------------------------
 *	@author Nico Funke
 *								2016
 *****************************************************/
#include "myCNF.h"
#include "myMatrix.h"
 
/**
 * Checks, if there are two 1-Clauses, which can not be fulfilled
 **/
 int isUnsolvable(CNF_t *cnf){	
 	int *v = createFullVector(cnf->variables, EMPTY);
 	int i = 0;
 	while(i < cnf->size){
		if(cnf->clauses[i][0] != EMPTY && cnf->clauses[i][1] == EMPTY){ 
		// is 1-clause
			if(v[abs(cnf->clauses[i][0]) - 1] == EMPTY){
				if ((cnf->clauses[i][0]) - 1 < 0){
					v[abs(cnf->clauses[i][0]) - 1] = FALSE;
				} else {
					v[abs(cnf->clauses[i][0]) - 1] = TRUE;
				} 
			} else {
				if((v[abs(cnf->clauses[i][0]) - 1] == FALSE && cnf->clauses[i][0] > 0)
					|| (v[abs(cnf->clauses[i][0]) - 1] == TRUE && cnf->clauses[i][0] < 0)){
					free(v);
					return TRUE;
				}
			}
		}
 		i++;
 	}
 	free(v);
 	return FALSE;			
 }

 /**
 * Skips a Line
 **/
void skipLine(FILE *input){
	while(fgetc(input) != '\n');
}

/**
 * frees the heap 
 **/
void freeCNF(CNF_t cnf){
	freeMatrixSimple(cnf.clauses,cnf.size);
}

/**
 * returns, whether a clause is fulfilled 
 **/
int clauseIsFulfilled(int *clause, int *solution, int kSAT){	
	int i = 0;
	while (i < kSAT){
		if(clause[i] != EMPTY){
			if(clause[i] < 0){	// neg
				if ( solution[abs(clause[i]) - 1] == FALSE ){
					return TRUE;
				}
			} else{				// pos
				if ( solution[abs(clause[i]) - 1] == TRUE ){
					return TRUE;
				}
			}
		} else {	// End of clause
			return FALSE;
		}
		i++;
	}
	return FALSE;
}

/**
 * constructs empty CNF, allocates Memory
 */
void createCNF( CNF_t *input, int clauses, int variables, int kSAT){	
	input->variables = variables;
	input->size 	 = clauses;
	input->kSAT 	 = kSAT;
	input->clauses = (int**) malloc (clauses * sizeof(int*));
	int i = 0;
	while( i < clauses ){
		input->clauses[i] = (int*) malloc (kSAT * sizeof(int));
		//insert 0
		int j=0;
		while(j<kSAT){
			input->clauses[i][j] = EMPTY;
			j++;
		}
		i++;
	}
}

/**
 * prints CNF
 **/
void printCNF(CNF_t input){		
	printf("Variables: %d\nClauses: %d\n" ,input.variables,input.size);
	int i = 0 ;
	while(i < input.size){
		int j = 0;
		while(j< input.kSAT){
			if( input.clauses[i][j] != EMPTY){
					printf("%d ",input.clauses[i][j]);
					j++;
			} else {
				j = input.kSAT;
			}
		}
		printf("0\n");
		i++;
	}
}

/**
 * checks if the input solves the CNF
 **/
int isSolution(CNF_t cnf, int *solution){	
	int i=0;
	while(i < cnf.size){
		if(clauseIsFulfilled( cnf.clauses[i], solution, cnf.kSAT) == FALSE){
			return 0;
		}
		i++;
	}
	return 1;
}


/**
 * Gets a CNF from the input
 * - returns 0, if an error occured 
 **/
int readCNFFromFile ( char *path, CNF_t *output, int kSAT){		
	FILE *input;

	input = fopen(path, "r");
	if(input == NULL){
		return 0;
	}

	char current[STRINGLENGTH];
	int variables,clauses, t;
	
// jump to problem line
	current[0] = fgetc(input);
	while(current[0] != 'p'){		
		skipLine(input);
		current[0] = fgetc(input);	
	}

	fscanf(input, "%s", current);
	if( current[0] != 'c' || current[1] != 'n' || current[2] != 'f'){
		fclose(input);
		return 0;
	}

	fscanf(input, "%d", &variables);
	fscanf(input, "%d", &clauses);

	createCNF(output,clauses,variables,kSAT);

// Fill clauses
	int i=0;
	int j=0;
	while(i<clauses){
		fscanf(input, "%d", &t);
		if(t == 0 || fgetc(input) == EOF){
			i++;
			j = 0;
		}else{
			output->clauses[i][j]= t;
			j++;
		}

	}
	
	fclose(input);
	return 1;
}

/**
 * Creates a random CNF (3-SAT)
 **/
void randomCNF(int vars, int clauses, CNF_t *cnf, int kSAT){
	//srand(time(NULL));
	createCNF(cnf,clauses,vars, kSAT);
	int i = 0;
	while(i<clauses){
		int j = 0;	
		int k = 0;		// position in Clause
		while(j < 3){
			int tmp = (rand()%(vars*2+1))-vars;
			if(tmp != 0){
				int t = 0;
				while (t < k){
					if( abs(cnf->clauses[i][t]) == abs(tmp)){
						cnf->clauses[i][t] = tmp; //negiert literal moeglicherweise
						t = k;
					}
					t++;
				}
				if ( t != k+1){		// Literal war noch nicht enthalten
					cnf->clauses[i][k] = tmp;
					k++;
				}
			}
			j++;			
		}
		qsort (cnf->clauses[i], k, sizeof(int), comp); // sort for same results as in older versions
		i++;
	}
}

/**
 * For the sorting algorithm
 * - compares the absolute values of a and b
 **/
int comp (const void * a, const void * b) {
    int aInt = abs(*((int*)a));
    int bInt = abs(*((int*)b));
    if (aInt > bInt) return  1;
    if (aInt < bInt) return -1;
    return 0;
}

/**
 * For sorting clauses
 * - compares the amount of literals
 * - returns 1 if they are the same 
 **/
int compClause (const void * aInput, const void * bInput) {
    int *a = *((int**) aInput);
    int *b = *((int**) bInput);
    int i = 0 ;
    while(i < kSATForSorting ){
    	if ( b[i] == EMPTY){  // a <= b
    		return 1;
    	} else if(a[i] == EMPTY) { // a > b
    		return -1;
    	}
    	i++;
    }
    return 1;
}

/**
 * Sortes the clauses in cnf, according to their amount of literals
 **/
void sortClauses (CNF_t *cnf){
	kSATForSorting = cnf->kSAT;
	qsort (cnf->clauses, cnf->size, sizeof(int *), compClause);
}