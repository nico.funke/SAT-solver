#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#define STRINGLENGTH  255
#define DEBUG		  0
#define TRUE		  1 
#define FALSE		  0
#define NOTCONTAINED -1
#define EMPTY		  0

#ifndef myCNF
#define myCNF
// ==== STRUCTS ==================================
typedef struct {
	int variables,size;
	int **clauses;
	int kSAT;
} CNF_t;

int kSATForSorting;	//Just for sorting the clauses

// ==== PROTOTYPES ===============================
void skipLine( FILE *input);
int isUnsolvable(CNF_t *cnf);
int readCNFFromFile ( char *path, CNF_t *output, int kSAT);
void createCNF( CNF_t *input, int clauses, int variables, int kSAT);
void printCNF(CNF_t input);
int isSolution(CNF_t cnf, int *solution);
int clauseIsFulfilled(int *clause, int *solution, int kSAT);
void freeCNF(CNF_t cnf);
void randomCNF(int vars, int clauses, CNF_t *cnf, int kSAT);
int comp (const void * elem1, const void * elem2);
int compClause (const void * aInput, const void * bInput);
void sortClauses (CNF_t *cnf);

#endif