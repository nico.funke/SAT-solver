SRC = solvingSat.c pointGenerator.c schoeningWalk.c myMatrix.c myCNF.c localSearch.c  randomPointGenerator.c
OBJ = ${SRC:.c=.o}
CC = clang
CCFLAGS =  -pg  -std=gnu99 -O0  


all:  solvingSat clean

solvingSat: solvingSat.o pointGenerator.o schoeningWalk.o myMatrix.o myCNF.o localSearch.o pointGenerator.o randomPointGenerator.o
	${CC}  ${CCFLAGS} -lm -pthread solvingSat.o pointGenerator.o schoeningWalk.o randomPointGenerator.o myMatrix.o  localSearch.o  myCNF.o  -o solvingSat

solvingSat.o: solvingSat.c
	${CC} ${CCFLAGS}  -c solvingSat.c

pointGenerator.o: pointGenerator.c
	${CC} ${CCFLAGS}  -c pointGenerator.c

randomPointGenerator.o: randomPointGenerator.c
	${CC} ${CCFLAGS}  -c  randomPointGenerator.c

schoeningWalk.o: schoeningWalk.c
	${CC} ${CCFLAGS}  -c  schoeningWalk.c

myMatrix.o: myMatrix.c
	${CC} ${CCFLAGS} -c myMatrix.c

myCNF.o: myCNF.c localSearch.o
	${CC} ${CCFLAGS} -c myCNF.c

localSearch.o: localSearch.c
	${CC} ${CCFLAGS} -c localSearch.c
	
clean:
	rm myMatrix.o solvingSat.o  localSearch.o schoeningWalk.o myCNF.o pointGenerator.o randomPointGenerator.o
