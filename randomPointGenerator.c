#include "randomPointGenerator.h"

/**
 * Creates a new RandomPointGenerator as in the paper
 **/
void newRandomPointGenerator(int var, int r, randomPointGenerator_t *gen){
	gen->var 	= var;
	gen->r 		= r;
	gen->p 		= getProbability(1.0*var);
 	gen->times	= getPointAmmount(1.0*var, 1.0*r);
 	//printf("%Lf\n", gen->times );
}

/**
 * Creates a new RandomPointGenerator with fixed amount of points k
 * !!! Use carefully, calculated probability may be wrong !!!
 **/
void newRandomPointGeneratorWithK(int var, int r, int k, randomPointGenerator_t *gen){
	gen->var 	= var;
	gen->r 		= r;
	gen->p 		= getProbabilityWithK(k,r,var);
	gen->times 	= k;
}

/**
 * Generates the next random Point at pointer point
 * returns 0 if there are no more ponts to generate
 **/
int getNextRandomPoint(randomPointGenerator_t *gen, int *point){
	int i = 0;
	gen->times = gen->times - 1;
	if(gen->times < 0){
		return 0;
	}
	generateRandomVector(gen->var, point);
	return 1;	
}
/**
 * Returns the probability, that a random code with k elements is 
 * a covering code for n variables with radius r
 * !!! very inaccurate & does not work in the most cases !!!
 **/
double getProbabilityWithK(double k, double r, double n){
	double tmp = pow(2,n);
	double vol = getVolume(n,r);

	return  1 - tmp * exp( (-1.0) * k * vol/tmp);
}
	
/**
 * Calculates the amount of points required for Corollary 1
 * from the paper
 **/
long double getPointAmmount(double n, double r){
	return n * beta(n, (r/n)) * pow(2, (1 - binaryEntropy(r,n) ) * n);
}

/**
 * Calculates the beta function from corollary 1 from the paper
 **/
double beta(double n, double p){
	return sqrt(n * p * (1.0 -p));
}

/** 
 * Returns the probability from the paper, that the random 
 * code is a covering code 
 **/
double getProbability(double n){
	return 1.0 - pow(2,n) * exp(-n);
}
/**
 * Calculates the binary entropy function with parameter p
 **/
double binaryEntropy(double r, double n){
	double p = r/n;
	double erg = -p * log2(p) - (1-p) * log2(1-p);
	return erg;
}

/**
 * Returns the volume of a hamming ball with n variables and 
 * radius r
 **/
double getVolume(double n, double r){
	double i = 0;
	double erg = 1;
	while ( i <= r){
		erg = erg + bin(n, i);
		i++;
	}
	return erg;
}

/**
 * returns the binomial coefficient ( n k )
 **/
double bin(double n, double k){
	return (fak(n)/ (fak(k) * fak(n-k)));
}

/**
 * retuns the factorial of x
 **/
double fak(double x){
	double erg = 1;
	while (x > 1){
		erg *= x;
		x--;
	}
	return erg;
}

/**
 * Prints a CSV output comparing the amount and probability 
 * of random points with the amount of points with covering codes 
 * for x <= n <= y
 **/
void printRandomPointAmountCSV(int x, int y){
	int i = x;
	double h = 1, g = 0,n,r;
	printf("vars;distance r;probability ;random points ;covering code points;is random better?\n");
	while (i <= y){
		if ( i > g*23 + h * 7){
			h++;
			if ( h > 2){
				h = 0;
				g++;
			}
		}
		r = h + 3*g;
		n = h*4 + g*12;
		double random = getPointAmmount(1.0*i,r);
		double covering = pow(2.0,n);
		printf("%d;%f;%f;%f;%f;%d;\n",i,r,getProbability(1.0*i),random ,covering, random < covering );
		i++;
	}
}
