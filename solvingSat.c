/*****************************************************
 *		Solving k-Sat with covering Codes
 *				and local search
 * 		---------------------------------
 *	@author Nico Funke
 *								2016
 *_____________________________________________________
 * NOTES
 * - read "README"
 ***************************************************/

#include <stdio.h>
#include <time.h>
#include <string.h>

#include "localSearch.h"
#include "myCNF.h"
#include "randomPointGenerator.h"
#include "schoeningWalk.h"

// ==== DEFINE ======================================

#define BILLION  1000000000L
 //For test types
#define TIME 	 1
#define STEPS 	 2
#define COVERINGCODES 1
#define RANDOMPOINTS 2
#define SCHOENINGWALK 3

// ==== VARS ========================================
int seed;
int printHelpVector = 0;
int maxThreads = 4;
int testcases = 100;
int variables = 7;
int minClauses = 0;
int maxClauses = 50;
int printDebugCNF = 0;
int kSAT = 3;
int maxVars;
int mode = COVERINGCODES;
int printProbability;
int sort = 0;
// --- time
struct timespec start, finish;
double elapsed;
char *file = NULL;


// ==== PROTOTYPES =====================================
void printLine();
void testProblems();
void testSingleProblem(char *file);
void printInformation();
void generatecsv(int type);

// ==== MAIN =====================================
int main(int argc, char *argv[]){
	//seed = 1478521769;
	printLine();
//---get information
	seed =time(NULL);
	int i = 1; 
	while(i < argc){
		if (strcmp(argv[i], "f") == 0) {
			file = argv[++i];
		}else if (strcmp(argv[i], "rcsv") == 0) {
			int x = atoi(argv[++i]);
			int y = atoi(argv[++i]);
			printRandomPointAmountCSV(x,y);
			return 1;
		} else if (strcmp(argv[i], "-sort") == 0){
			sort = 1;
		} else if (strcmp(argv[i], "s") == 0){
			seed = atoi(argv[++i]);
	  	} else if (strcmp(argv[i], "r") == 0){
	  		mode = RANDOMPOINTS;
			printProbability = atoi(argv[++i]);
		} else if (strcmp(argv[i], "sch") == 0){
	  		mode = SCHOENINGWALK;
			printProbability = atoi(argv[++i]);
	  	} else if (strcmp(argv[i], "-d") == 0){
	  		printHelpVector = 1;
	  	} else if (strcmp(argv[i], "-cnf") == 0){
	  		printDebugCNF = 1;
	  	} else if (strcmp(argv[i], "c") == 0){
	  		kSAT = atoi(argv[++i]);
	  	} else if (strcmp(argv[i], "t") == 0){
	  		variables = atoi(argv[++i]);
	  		testcases = atoi(argv[++i]);
	  		minClauses = atoi(argv[++i]);
	  		maxClauses = atoi(argv[++i]);
	  	} else if (strcmp(argv[i], "csv") == 0){
	  		variables = atoi(argv[++i]);
	  		testcases = atoi(argv[++i]);
	  		minClauses = atoi(argv[++i]);
	  		maxClauses = atoi(argv[++i]);
	  		maxVars = atoi(argv[++i]);
	  		int type;
	  		if ((strcmp(argv[++i], "t")) == 0){
	  			type = TIME;
	  		} else if((strcmp(argv[i], "s")) == 0){
	  			type = STEPS;
	  		}else{
	  			printf("Fehlende Eingabe, ob Zeit oder Schritte ausgegeben werden sollen\n");
	  			return 1;
	  		}
	  		generatecsv(type);
	  		return 0;
	  	} else if (strcmp(argv[i], "mt") == 0){
	  		maxThreads = atoi(argv[++i]);
	  	}else{
	  		printf("Ungueltiger Parameter: \"%s\"\n", argv[i]);
	  		return 1;
	  	}
	  	i++;
  	}


//--- calc
  	srand(seed);
	printf("Threads: %d\n", maxThreads );
	
	clock_gettime(CLOCK_MONOTONIC, &start);
	if(file ==NULL){
		testProblems();
	}else{
		testSingleProblem(file);
	}
	clock_gettime(CLOCK_MONOTONIC, &finish);

//--- end
	elapsed = (finish.tv_sec - start.tv_sec);
	elapsed += (finish.tv_nsec - start.tv_nsec)/(double)BILLION;
	
	printf("Zeit: %fs\n",elapsed);	
	printLine();

	return 0;
}

//===== METHODS FOR LOCAL SEARCH ==============================================


/**
 * Just for better readability of the output
 **/
void printLine(){
	printf("=============================================\n");
}

/**
 * prints information about the tests
 **/
void printInformation(){
	printf("Testcases: %d\n",testcases );
	printf("Variables: %d\n",variables );
	printf("Seed: %d\n",seed);
	printf("Max. Clauses: %d\n", maxClauses);
	printf("Max. Clausesize: %d\n", kSAT);
	if(mode == COVERINGCODES){
		printf("Mode: coveringCodes");
	} else if (mode == RANDOMPOINTS){
		printf("Mode: randomPoints");
	} else if( mode == SCHOENINGWALK){
		printf("Mode: schoeningWalk");
	} else {
		printf("Mode: unknown");
	}
	if(sort){
		printf("(sorted clauses)\n" );
	}else{
		printf("\n");
	}
	printLine();
}

/**
 * Solves a single Problem, given as a file
 **/
void testSingleProblem(char *file){ ///
	CNF_t cnf;
	
	readCNFFromFile(file, &cnf, kSAT);
	if(sort){
		sortClauses(&cnf);
	}
	if(printDebugCNF){
		printf("Solving CNF: \n");
		printCNF(cnf);
	}
	printLine();
	int steps = 0;
	int *solution;
	if(mode == RANDOMPOINTS){
		solution = solveSatWithGenerator(cnf,NULL,printProbability,&steps, maxThreads, printHelpVector);
	} else if(mode == COVERINGCODES){
		solution= simplelocalSearchSolver(cnf, &steps, maxThreads, printHelpVector);
	} else if(mode == SCHOENINGWALK){
		solution = solveWithSchoeningWalk(&cnf, &steps, printProbability);
	}
	printf("Steps: %d\nLoesung: ",steps );
	printVector(solution, cnf.variables);
}

/**
 * Tests the solvers multiple times with random CNFs
 * - parameter as defined at the begin of file
 **/
void testProblems(){
	printInformation();
	int i = minClauses, steps = 0;
	CNF_t cnf;
	int *solution;

	printf("Klauseln\tSchritte\tUnloesbar\n")	;
	while(i < maxClauses){
		int j = 0,ls = 0, vectorlength =0, unsolv = 0;
		while(j < testcases){
			randomCNF(variables,i,&cnf, kSAT);

			if(sort)	sortClauses(&cnf);	
			if(printDebugCNF) printCNF(cnf);
			
			steps = 0;
			if(mode == RANDOMPOINTS){
				solution = solveSatWithGenerator(cnf,NULL,printProbability,&steps, maxThreads, printHelpVector);
			} else if(mode == COVERINGCODES) {
				solution = simplelocalSearchSolver(cnf,&steps, maxThreads, printHelpVector);
			} else if( mode == SCHOENINGWALK ){
				solution = solveWithSchoeningWalk(&cnf, &steps, printProbability);
			}
			if(printDebugCNF)printVector(solution, cnf.variables);
			if(solution != NULL){
				free(solution);
			} else {
				unsolv++;
			}
			ls += steps;
			j++;
			freeCNF(cnf);
		}

		printf("%d \t\t%d\t\t%d\n", i , ls/testcases,unsolv);
		i++;
	}
	printLine();
}

/**
 * Does multiple test and prints the number of steps or the time
 * in csv-format
 * -input must be either STEPS or TIME
 **/
void generatecsv(int type){
	printInformation();
	int i = minClauses, steps = 0, k = variables;
	CNF_t cnf;
	int *solution;

	printf("vc;");
	while(i <= maxClauses){
		printf("%d;",i );
		i++;
	}
	printf("\n");
	i = minClauses;

	// i = Clauses
	// j = testcases
	// k = variables
	// ls = overall steps
	while(k <= maxVars){
		printf("%d;",k );
		i = minClauses;
		while(i <= maxClauses){
			int j = 0,ls = 0;
			// to count time
			struct timespec s, f;
			double e = 0.0;

			while(j < testcases){
				randomCNF(k,i,&cnf,kSAT);

				if(sort)	sortClauses(&cnf);				
				if(printDebugCNF) printCNF(cnf);
				
				steps = 0;
				clock_gettime(CLOCK_MONOTONIC, &s);
				solution = simplelocalSearchSolver(cnf,&steps, maxThreads, printHelpVector);
				clock_gettime(CLOCK_MONOTONIC, &f);
				e += (f.tv_sec - s.tv_sec);
				e += (f.tv_nsec - s.tv_nsec)/(double)BILLION;

				if(solution != NULL){
					free(solution);
				}
				ls += steps;
				freeCNF(cnf);
				j++;
			}
			if(type == TIME){	//counting time
				printf("%f;",e/testcases);
			} else if(type == STEPS){ // counting steps
				printf("%d;",ls/testcases);
			}
			i++;
		}
		k++;
		printf("\n");
	}


}
