/*****************************************************
 *		 Solving SAT with localsearch
 * 		---------------------------------
 *	@author Nico Funke
 *								2016
 *****************************************************/
#include "localSearch.h"
#include "pointGenerator.h"
#include "randomPointGenerator.h"

/**
 * Returns the first false clause under solution in cnf
 * - returns NULL if there is no clause and prints an error message
 **/
int *getFalseClause(CNF_t cnf, int *solution){
	int i = 0;
	while ( i < cnf.size){
		if (clauseIsFulfilled(cnf.clauses[i], solution, cnf.kSAT) == FALSE){
			return cnf.clauses[i];
		}
		i++;
	}
	printf("Error: all clauses are fulfilled. There is no variable that needs to be changed\n");
	return NULL;
}

/**
 * Returns vector, whether a variable needs to change for cnf 
 * - value == 1 => variable can be changed
 * - returns vector on heap
 *  !!! WORKS WITH THE CNF STRUCT AS IN VERSION 2 !!!
 */
void variablesToChange( CNF_t cnf, int *solution, int *output){		
	int i = 0;
	while(i < cnf.variables){
		output[i] = FALSE;
		i++;
	}
	i = 0;
	while(i < cnf.size){
		if( !(clauseIsFulfilled(cnf.clauses[i], solution, cnf.kSAT)) ){
			// clause is not yet fulfilled
			int j = 0;
			while (j < cnf.variables){
				if(cnf.clauses[i][j] != EMPTY){
					// variable is in clause
					output[j] = TRUE;
				}
				j++;
			}
			return;
		}
		i++;
	}
	printf("Error: all clauses are fulfilled. There is no variable that needs to be changed\n");
}

/**
 * fills an localSearchInputPackage with the given values
 * - allocates memory for changedVariables	
 **/
void newInputpackage(localSearchInputPackage *input, int depth, 
	CNF_t cnf, int *solution, int *steps, int *isSolved,int *threadFree ){

	input->depth = depth;
	input->cnf = cnf;
	input->solution = solution;
	input->steps = steps;
	input->isSolved = isSolved;
	input->threadFree = threadFree;

	//changedVariables
	input->changedVariables = createFullVector(cnf.variables, FALSE);
}

/**
 * frees the allocated memory from a localSearchInputPackage
 **/
void freeInputPackage(localSearchInputPackage *input){
	free(input->changedVariables);
}

/**
 * starts localsearch at a given point
 * - isSolved says, if the Problem is solved
 * - isSolved = 0, if no solution was found	(solution will not be overwritten)
 * - if isSolved = 1, solution is a solution  (solution will be overwritten)
 * - counts steps in steps
 **/
void *localSearchAtPoint(localSearchInputPackage *input){
	//--- extract package
	int *steps = input->steps;
	(*steps)++;
	int *isSolved = input->isSolved;
	if((*isSolved)){
		return NULL;
	}
	int depth = input->depth;
	int* changedVariables = input->changedVariables; 
	CNF_t cnf = input->cnf; 
	int *solution = input->solution; 	
	
	if(SHOWWAYY) printVector(solution,cnf.variables);

	if(isSolution(cnf, solution)){
		(*isSolved) =TRUE;;
		return NULL;
	}
	if(depth == 0){
		return NULL;
	}
	int i = 0;
	int *changes = getFalseClause(cnf, solution);
	while(i < cnf.kSAT){

		if((*isSolved)){
			return NULL;
		}

		if( changes[i] != EMPTY){
			int j = abs(changes[i]) - 1;
			solution[j] = (solution[j]+1)%2; 	//Flip Bit
			int newDepth = depth;
			if(changedVariables[j] == FALSE){
				//if var was not changed yet
				changedVariables[j] = TRUE;
				newDepth--;
				input->depth = newDepth;	//set new depth
				localSearchAtPoint(input);
				input->depth = depth;		// back to old depth
				if( (*isSolved)){
					return NULL;
				}
			}				
			solution[j] = (solution[j]+1)%2; //Flip back
			i++;
		} else {	// end of clause
			i = cnf.kSAT;
		}
	}
	return NULL;
}

/**
 * Generates generatormatrix and solves SAT with localsearch
 **/
int *simplelocalSearchSolver(CNF_t cnf,int *steps, int maxThreads, int printHelpVector){
	//---choose codes
	int hamming = 0, golay = 0;
	while((hamming*HAMMINGVARS + golay*GOLAYVARS) < cnf.variables){
		if(hamming == 2){
			hamming = 0;
			golay++;
		}else{
			hamming++;
		}
	}
	int solLength = (hamming*HAMMINGVARS + golay*GOLAYVARS);
	//---create solution
	int *solution;

	int i;
	matrix_t generator, HammingGenerator, GolayGenerator;
	fileToMatrix(HAMMINGGENERATOR, &HammingGenerator);
	fileToMatrix(GOLAYGENERATOR, &GolayGenerator);
	newMatrix( &generator, 0,0);
	  //add golays
	i = 0;;
	while(i < golay){
		matrix_t tmp, tmp2;
		combineGenerator(generator,GolayGenerator,&tmp);
		tmp2 = generator;
		generator = tmp;
		freeMatrix(&tmp2);
		i++;
	}

	  //add hammings
	i = 0;
	while(i < hamming){
		matrix_t tmp, tmp2;
		combineGenerator(generator,HammingGenerator,&tmp);
		tmp2 = generator;
		generator = tmp;
		freeMatrix(&tmp2);
		i++;
	}

	//---solve problem
	int depth = HAMMINGDEPTH*hamming + GOLAYDEPTH*golay;
	int l;
	//---free heap
	freeMatrix(&HammingGenerator);
	freeMatrix(&GolayGenerator);
	solution = solveSatWithGenerator(cnf,&generator,depth,steps, maxThreads, printHelpVector);

	return solution;
}

/**
 * Solves SAT with local search
 * - uses random points, if gen = NULL and depth as a boolean value, whether 
 *   probabilities should be printed or not
 * -allocates heap for solution
 **/
int *solveSatWithGenerator(CNF_t cnf, matrix_t *gen,int depth, int *steps, 
	int maxThreads, int printHelpVector){
	
	matrix_t generator;
	int random = 0;
	int printProbability = 0;
	if(gen != NULL){
		generator = *gen;
	} else {
		random = 1;
		printProbability = depth;
	}
	//--- simple check
	//if(isUnsolvable(&cnf)){
	//	return NULL;
	//}
	pthread_t p[maxThreads];
	// ---
	int **solution = (int**) malloc(maxThreads * sizeof(int*));
	int i = 0;
	int canStillBeIncremented = 1;
	int step[maxThreads];
	while(i < maxThreads){
		if(random){
			solution[i] = (int *) malloc(cnf.variables * sizeof(int));
		}else{
			solution[i] = (int *) malloc(generator.m * sizeof(int));
		}
		step[i] = 0;
		i++;
	}
	
	pointGenerator_t pGenerator;
	randomPointGenerator_t rGenerator;
	if(random){
		// --- calculate r
		/*int var = cnf.variables;
		int r= 3 * (var/23);
		var = var%23;
		if (var < 22){
			r = r + (var+6)/7;
		} else {
			r = r + 3;
		}*/
		double r = cnf.variables * 0.25;
		depth = (int)(r+1);
		depth = 11;
		newRandomPointGenerator(cnf.variables, depth, &rGenerator);
	}else{
		newPointGenerator(&pGenerator, &generator);	
	}
	
	do{

		int isSolved = 0;

		//---Multithreading
		int actualThreads = 0;
		localSearchInputPackage input[maxThreads];	

		while( actualThreads < maxThreads && canStillBeIncremented){
			if(random){
				canStillBeIncremented = getNextRandomPoint(&rGenerator,solution[actualThreads]);
			}else{
				canStillBeIncremented = getNextPoint(&pGenerator, solution[actualThreads],printHelpVector);
			}
			if(canStillBeIncremented){
				newInputpackage( &input[actualThreads], depth, cnf, 
					solution[actualThreads], &(step[actualThreads]), &isSolved, NULL);
				pthread_create (&p[actualThreads], NULL, 
					(void *(*)(void *))localSearchAtPoint, &input[actualThreads]);
				actualThreads++;
			}
		}
		i = 0;
		while(i < actualThreads){
			pthread_join(p[i],NULL);
			i++;
		}

		//--- free input packages
		i=0;
		while(i < actualThreads){
			freeInputPackage(&input[i]);
			i++;
		}	
		//--- check if problem is solved
		if(isSolved){
			int j = -1;	
			i = 0;		
			while(i < maxThreads){

				if(isSolution(cnf, solution[i])){
					j = i;
				}
				i++;
			}
			if( j == -1) printf("ERROR: Can not find solution\n");

			i = 0;
//			printCNF(cnf);
//			printVector(solution[j], cnf.variables);
			while( i < maxThreads){
				if( i != j){
					free(solution[i]);
				}
				(*steps)+= step[i];
				i++;
			}
			int *output = solution[j];
			free(solution);
			if(!random){
				freePointGenerator(&pGenerator); 
			}
				//printf("loesbar :)\n");
			return output;
			
		}	
	}while(canStillBeIncremented);
	if(!random){
		freePointGenerator(&pGenerator);
	}
	i = 0;
	while(i < maxThreads){
		free(solution[i]);
		(*steps)+= step[i];
		i++;
	}
	free(solution);
	if(printProbability){
		printf("Probability: %f\n", getProbability(cnf.variables));
	} 
		//printf("nicht loesbar :/\n");
	return NULL;
}