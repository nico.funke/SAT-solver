#include <stdio.h>
#include <stdlib.h>
#include <math.h>


#define TRUE		  1 
#define FALSE		  0
#define NOTCONTAINED -1
#define EMPTY		  0

#ifndef schoeningWalk
#define schoeningWalk

#include "myMatrix.h"
#include "myCNF.h"

int *solveWithSchoeningWalk(CNF_t *cnf, int *steps, int printProbability);
int getRandomLiteral(int *clause,int kSAT);
int *getRandomFalseClause(CNF_t *cnf, int *solution);
double getSchoeningSuccessProbability(int kSAT, int var);
double calculateTimes(int kSAT, int var);
double getSchoeningErrorProbability(double times, double probability);
#endif