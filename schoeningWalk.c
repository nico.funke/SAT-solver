/*********************************************************
 *		  Implementation of schoenings algorithm
 * 		------------------------------------------
 *	@author Nico Funke
 *								2016
 *****************************************************/

 #include "schoeningWalk.h"

/**
 * Searches for a solution for the cnf with schoenings walk.
 * - Returns a solution or NULL, if no solution was found
 **/
 int *solveWithSchoeningWalk(CNF_t *cnf, int *steps, int printProbability){
	double times = calculateTimes(cnf->kSAT, cnf->variables);
	double j = 0;
	while(j < times){
		// Generate a random vector to start at
		int *solution = (int *) malloc( sizeof(int) * cnf->variables);
		generateRandomVector(cnf->variables, solution);
		if(isSolution(*cnf, solution)){
			return solution;
		}

		// Do the walk
		int i = 0;
		int times = (cnf->variables) * 3;
		while( i < times){		
			if(isSolution(*cnf, solution)){
				(*steps)++;
				return solution;
			}
			int *clause = getRandomFalseClause(cnf, solution);
			int literal = getRandomLiteral(clause, cnf->kSAT);
			if(literal == -1){
				break;
			}
			solution[literal] = (solution[literal]+1) % 2; //flip var
			(*steps)++;
			i++;
		}
		j = j+1;
	}
	if(printProbability){
		double success = getSchoeningSuccessProbability(cnf->kSAT,cnf->variables);
		printf("ErrorProbability: %f\n", getSchoeningErrorProbability(times,success));
	}
	return NULL;
}

/**
 * Returns a random literal from the given clause
 * with zero as base variable
 **/
int getRandomLiteral(int *clause,int kSAT){
	if (clause[0] == EMPTY){
		return -1;
	}
	int i = rand() % kSAT;
	while(clause[i] == EMPTY){
		i = rand() % kSAT;
	}
	return abs(clause[i]) - 1; // -1 for base zero
}

/**
 * Looks randomly for a not yet fulfilled clause
 **/
int *getRandomFalseClause(CNF_t *cnf, int *solution){
	int i = rand() % cnf->size;
	if(isSolution(*cnf,solution)){
		printf("ERROR: Cannot find false clause in a solved cnf!");
	}
	while(clauseIsFulfilled(cnf->clauses[i], solution, cnf->kSAT)){
		i = (i+1) % cnf->size;
	}
	return cnf->clauses[i];
}

/**
 * Returns the success probability of schoenings algorithm
 **/
double getSchoeningSuccessProbability(int kSAT, int var){
	return pow((0.5 * (1.0+(1.0/(kSAT-1.0)))), 1.0*var);
}

/**
 * Returns the times how often the procedure has to be repeaten
 **/
double calculateTimes(int kSAT, int var){
	return 2.0/getSchoeningSuccessProbability(kSAT,var);
}

/**
 * Returns the error probability
 **/
double getSchoeningErrorProbability(double times, double probability){
	return exp(-20.0 * probability * times);
}