#include <stdio.h>
#include <stdlib.h>
#include <math.h>


#define TRUE		  1 
#define FALSE		  0
#define NOTCONTAINED -1

#ifndef randomPointGenerator
#define randomPointGenerator

#include "myMatrix.h"

typedef struct{
	long double times;
	double p;
	int var;
	int r;
} randomPointGenerator_t;

double fak(double x);
double bin(double n, double k);
double getVolume(double n, double r);
double getProbabilityWithK(double k, double r, double n);
double binaryEntropy(double r, double n);
double getProbability(double n);
long double getPointAmmount(double n, double r);
double beta(double n, double p);
void printRandomPointAmountCSV(int x, int y);
void newRandomPointGenerator(int var, int r, randomPointGenerator_t *gen);
void newRandomPointGeneratorWithK(int var, int r, int k, randomPointGenerator_t *gen);
int getNextRandomPoint(randomPointGenerator_t *gen, int *point);
#endif