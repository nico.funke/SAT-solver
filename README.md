## Solving k-SAT with covering codes
Institut fuer Informatik Göttingen

Programm fuer die Bachelorarbeit Wintersemester 2016/17

**Author**: Nico Funke

**Betreuer**: Prof. Damm

## Informationen
Das Programm loest CNFs mittels lokaler Suche ueber durch Hamming-und Golaycodes erzeugte
Punkte.

## Anleitung
Das Programm laesst sich auf drei verschiedene Weisen starten:

1.	**Dateibetrieb**
	Loest die in der ubergebenen *.cnf-Datei enthaltene CNF

2.	**Testbetrieb**
	Wird automatisch ausgefuehrt, falls keine Datei angegeben wird.
	Loest fuer jede Anzahl an Klauseln mehrere zufaellig generierte CNFs und berechnet den
	Mittelwert der benoetigten Schritte fuer diese Klauselzahlen.

3.	**Generieren von csv Ausgaben**
	3.1 Arbeitet wie der Testbetrieb, erhöht jedoch auch die Anzahl der Variablen und
	gibt eine Tabelle im csv Format aus.
	Es kann gewaehlt werden, ob die Zeit oder die ANzahl der Schritte ausgegeben werden soll.
	3.2 Gibt eine Auflistung der benoetigten Punkte fuer ueberdeckungscodes und
	Zufallspunkte, so wie deren Wahrscheinlichkeit und die Suchtiefe aus.

## STEUERUNG
| Parameter | Wirkung |
|-----------|-----------------------------------------------------------------------------|
 	f 	|	Startet Dateibetrieb, erwartet im naechsten Argument einen Dateipfad
 	t 	|	Startet Testbetrieb, erwartet als naechstes die Variablenanzahl, die Anzahl der Testdurchlaeufe pro Klauselanzahl, die Anzahl an Klauseln zu Beginn und die Anzahl der Klauseln zum Ende als Argument
 	mt |		aendert die Anzahl an Threads, erwartet als naechsten Parameter die Anzahl zu benutzender Threads
 	-d 	|	aktiviert die Ausgabe der Hilfsvektoren, die mit der Generatormatrix multipliziert werden (Aendert nichts bei zufaellig generierten Punkten)
 	-cnf |	aktiviert die Ausgabe der CNFs vor dem loesen eines Problems und Ausgabe 		 der Loesung
 	-sort	|Die Klauseln der CNF werden vorsortiert, so dass die kleinsten Klauseln am Anfang stehen
 	s  |		Veraendert den Seed fuer die zufällig erzeugten CNFs, erwartet im naechsten Argument einen Seed (veraendert nicht den Dateibetrieb)
 	csv |	Startet csv-Betrieb. Erwartet als naechstes gleiche Parameter Testbetrieb und zusaetzlich die maximal Variablenanzahl und ein 't'(Zeit) oder ein 's'(Schritte), ob Zeit oder Schritte ausgegeben werden sollen.
 	c 	|	Aendert die maximale Klauselgroesse, erwartet als nächstes eine Zahl mit der maximalen Klauselgroesse
	r 	|	Stellt die Generierung der Punkte auf zufaellig generierte Punkte um. Erwartet als naechsten Parameter eine eins oder null, ob die Korrektheitswahrscheinlichkeit bei unloesbaren Problem mit ausgegeben werden soll
	rcsv	|Gibt eine Auflistung der benoetigten Punkte fuer ueberdeckungscodes und Zufallspunkte, so wie deren Wahrscheinlichkeit aus Erwartet als naechsten beiden Argumente die Start -und Endanzahl Variablen
	sch 	|Loest die Probleme mithilfe des Algorthmus von Erwartet als naechsten Parameter eine eins oder null, ob Wahrscheinlichkeit bei ungeloesten Problemen mit werden sollen.

### Standardwerte
Ohne Argumente startet das Programm im Testbetrieb mit

| Parameter | Wert |
|---------------------|----------------|
Threads:	|			4
Testfaelle pro CNF: |	100
Seed:					|time(NULL)
Variablen:				|7
Maximale Klauseln:		|50
Maximale Klauselgroesse |3
