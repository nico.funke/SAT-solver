#include "myMatrix.h"
#include "myCNF.h"
#include <pthread.h>

#define TRUE		  		1 
#define FALSE		  		0
#define NOTCONTAINED 		-1

#define SHOWWAYY				0
#define SHOWWAY 			0
#define PRIME				73

#define HAMMINGGENERATOR 	"MAT/HammingGenerator.mat"
#define HAMMINGDEPTH		1
#define HAMMINGVARS			7

#define GOLAYGENERATOR		"MAT/GolayGenerator.mat"
#define GOLAYDEPTH			3
#define GOLAYVARS			23



typedef struct{
	int depth;
	int* changedVariables; 
	CNF_t cnf; 
	int *solution; 
	int *steps;
	int *isSolved;
	int *threadFree;
} localSearchInputPackage;

void variablesToChange( CNF_t cnf, int *solution, int *output);
void *localSearchAtPoint(localSearchInputPackage *input);
void newInputpackage(localSearchInputPackage *input, int depth, 
	CNF_t cnf, int *solution, int *steps, int *isSolved,int *threadFree );
void freeInputPackage(localSearchInputPackage *input);
int *solveSatWithGenerator(CNF_t cnf, matrix_t *generator,int depth, int *steps, 
	int maxThreads, int printHelpVector);
int *simplelocalSearchSolver(CNF_t cnf,int *steps, int maxThreads, int printHelpVector);

int *getFalseClause(CNF_t cnf, int *solution);