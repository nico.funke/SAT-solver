/*****************************************************
 *		Operations with matrices & vectors
 * 		---------------------------------
 *	@author Nico Funke
 *								2016
 *****************************************************/

#include "myMatrix.h"

/**
 * Calculates a+b and saves the result under solution and b
 **/
void addVectorsMod2(int* a, int* b, int* solution, int length){
	int i = 0;
	while( i < length){
		solution[i] = (a[i] + b[i])%2;
		b[i] = solution[i];
		i++;
	}
}

/**
 * fills the vector with given length randomly with 0 and 1
 **/
void generateRandomVector(int length, int *vector){
	int i = 0;
	while (i < length){
		vector[i] = rand() % 2;
		i++;
	}
}

/**
 * Conbines two generator matrices
 * Matrix A, Matrix B
 * as (A 0)
 *    (0 B)
 **/
void combineGenerator(matrix_t first, matrix_t second, matrix_t *solution){
	newMatrix(solution,first.n+second.n,first.m+second.m);
	int i = 0;
	while(i < solution->n){
		int j=0;
		while(j < solution->m){
			if( i < first.n && j < first.m){
				solution->field[i][j] = first.field[i][j];
			} else if ( i > first.n && j >first.m){
				solution->field[i][j] = second.field[i-first.n][j-first.m];
			} else{
				solution->field[i][j] = 0;
			}
			j++;
		}
		i++;
	}
}

/**
 * Allocates Memory on Heap for matrix_t 
 **/
void newMatrix(matrix_t *matrix, int n, int m){
	matrix->n = n;
	matrix->m = m;
	matrix->field = (int**) malloc( n * sizeof(int*));
	int i=0;
	while(i < n){
		matrix->field[i] = (int*) malloc(m * sizeof(int));
		i++;
	}
}

/**
 * Multiplies matrix and vector mod 2
 **/
void matrixMultVectorMod2(int *vector, matrix_t matrix, int *solution){
 	int i = 0;
 	while ( i < matrix.m){
 		int j = 0;
 		solution[i] = 0;
 		while( j < matrix.n){
 			solution[i] += vector[j] * matrix.field[j][i];
 			j++;
 		}
 		solution[i] = solution[i] % 2;
 		i++;
 	}
}

/**
 * increments a binary vector
 * -returns the position of the first zero
 * - returns -1 if the vector can not be incremented
 **/
int incrementVector(int *vector, int length){
	int i = length-1;
	while(vector[i] != 0){
		vector[i] = 0;
		i--;
		if(i < 0){
			return -1;
		}
	}
	vector[i] = 1;
	return i;
}
/**
 * frees Heap
 **/
void freeMatrix( matrix_t *matrix){
	freeMatrixSimple( matrix->field, matrix->n);
}

/**
 * returns, whether a vector just contains zeroes
 **/
int isEmpty(int *vector, int length){
	int i = 0;
	while(i < length){
		if(vector[i] != 0){
			return 0;
		}
		i++;
	}
	return 1;
}

/**
 * prints matrix
 **/
void printMatrix(matrix_t matrix){
	printf("%d %d\n", matrix.n, matrix.m );
	int i=0;
	while(i<matrix.n){
		int j=0;
		while(j<matrix.m){
			printf("%d ", matrix.field[i][j]);
			j++;
		}
		printf("\n");
		i++;
	}
}

/**
 * prints a vector
 **/
void printVector(int *vector, int length){
	int i = 0;
	if(vector == NULL){
		printf("NULLVECTOR\n");
		return;
	}
	while(i < length){
		printf("%d",vector[i]);
		i++;
	}
	printf("\n");

}
/**
 * converts file into int**
 **/
void fileToMatrix(char* path, matrix_t *matrix){
	FILE *input;

	input = fopen(path, "r");

	fscanf(input,"%d",&matrix->n);
	fscanf(input,"%d",&matrix->m);
	matrix->field = (int **) malloc(matrix->n*sizeof(int*));
	int i = 0;

	while(i < matrix->n){
		int j = 0;
		matrix->field[i] = (int *) malloc(matrix->m*sizeof(int));
		while(j < matrix->m){
			fscanf(input, "%d", &matrix->field[i][j]);
			j++;
		}
		i++;
	}

	fclose(input);
}

/**
 * multiplicates two matrices x*y=z 
 * - result is mod(2)
 **/
void matrixMultMod2(matrix_t x, matrix_t y, matrix_t *z){
	z->field = (int**) malloc(x.n*sizeof(int*));
	z->n = x.n;
	z->m = y.m;
	int i=0;
	while(i < x.n){
		z->field[i] = (int*) malloc(y.m*sizeof(int*));
		int j=0;
		while(j < y.m){
			z->field[i][j] = 0;
			int k 	= 0;
			while( k < y.n){
				z->field[i][j] += x.field[i][k] * y.field[k][j];
				k++;
			}
			z->field[i][j] = z->field[i][j] % 2;
			j++;
		}
		i++;
	}
}

/**
 * frees Heap
 **/
void freeMatrixSimple( int **matrix, int n){
	int i = 0;
	while( i < n ){
		free(matrix[i]);
		i++;
	}
	free(matrix);
}

/**
 * returns, whether x and y are equal
 **/
int vectorEquals(int *x, int *y, int length){
	int i = 0;
	while(i < length){
		if(x[i] != x[i]){
			return 0;
		}
		i++;
	}
	return 1;
}

/**
 * calculates Hamming distance 
 **/
int hammingDistance(int *x, int *y, int length){
	int i = 0, distance = 0;
	while (i < length){
		if(x[i] != y[i]){
			distance++;
		}
		i++;
	}
	return distance;
}

/**
 * Creates a new Vector Filled with the value value
 * - returns allocated memory on heap
 **/
int *createFullVector(int length, int value){
	int *output = (int*)malloc(sizeof(int) * length);
	int i=0;
	while(i<length){
		output[i] = value;
		i++;
	}
	return output;
}