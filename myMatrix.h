#include <stdio.h>
#include <stdlib.h>

#define TRUE		  1 
#define FALSE		  0
#define NOTCONTAINED -1

#ifndef myMatrix
#define myMatrix

typedef struct {
	int n,m;
	int **field;
} matrix_t;


void matrixMultVectorMod2(int *vector, matrix_t matrix, int *solution);

void matrixMultMod2(matrix_t x, matrix_t y, matrix_t *z);
void freeMatrix( matrix_t *matrix);
void printMatrix(matrix_t matrix);
void fileToMatrix(char* path, matrix_t *matrix);
void freeMatrixSimple( int **matrix, int n);
int isEmpty(int *vector, int length);
int vectorEquals(int *x, int *y, int length);  		// UNUSED?
int hammingDistance(int *x, int *y, int length);	// UNUSED?
void printVector(int *vector, int length);
int incrementVector(int *vector, int length);
void newMatrix(matrix_t *matrix, int n, int m);
void combineGenerator(matrix_t first, matrix_t second, matrix_t *solution);
void addVectorsMod2(int* a, int* b, int* solution, int length);
int *createFullVector(int length, int value);
void generateRandomVector(int length, int *vector);


#endif